FROM php:7.4-fpm

# Install Dependencies
RUN apt-get update && apt-get install -y \
        libpng-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libjpeg-dev \
        libpq-dev \
        libpq5 \
        libjpeg62-turbo \
        libfreetype6 \
        git \
        wget \
        unzip \
        libmagickwand-dev \
        libgd-dev \
        cron \
	    libwebp-dev \
	    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
	    && docker-php-ext-install -j$(nproc) gd

RUN docker-php-ext-install pdo_mysql mysqli && \
    docker-php-ext-install pdo_pgsql && \
    docker-php-ext-install exif && \
    pecl install imagick && \
    docker-php-ext-enable imagick

# Include Woltlab Setup
# RUN wget -O /tmp/woltlab.zip https://assets.woltlab.com/release/woltlab-suite-5.2.9.zip && \
#    unzip /tmp/woltlab.zip -d /tmp/woltlab && \
#    mkdir -p /app && \
#    mkdir -p /opt/woltlab && \
#    mv /tmp/woltlab/upload/* /app

# Setup crontab
# COPY cron.php /opt/woltlab/cron.php
# COPY crontab /etc/cron.d/woltlab-cron
# RUN chmod 0644 /etc/cron.d/woltlab-cron
# RUN crontab /etc/cron.d/woltlab-cron

# COPY entrypoint.sh /etc/entrypoint.sh

EXPOSE 80
WORKDIR /app
# ENTRYPOINT ["sh", "/etc/entrypoint.sh"]
