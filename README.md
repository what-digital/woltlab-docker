# Woltlab Server

## Setup

- git clone this repository to a server
- execute `touch infrastructure/traefik.acme.json && chmod 600 infrastructure/traefik.acme.json`
- add the woltlab setup files to app/ 
   - install.php
   - test.php
   - WCFSetup.tar.gz
- create the .env file and set the secrets (copy .env-example)
- Install docker and docker-compose and execute `docker-compose up -d`


## Notes

- local url: See the .env file (default value: https://woltlab.127.0.0.1.nip.io/)
- Password reset: in the table wcf1_user replace a user's password with `$2a$08$X1yxWDYrpTCcdIpXK1Ug/ujWsup8o/8SPsLRGM/Yth1DX8bP./zUi`. The password is thereafter: `root`
- clamav:
  - you can scan the app directory manually like this: `docker-compose exec clamav clamscan -r -i /app/ --remove`
  - cron setup: add this to your host system's cron: `* * * * * cd /srv/woltlab-docker/ && docker-compose exec clamav clamscan -r -i /app/ --remove`
- for the traefik dashboard at https://woltlab.127.0.0.1.nip.io/dashboard set BASIC_AUTH in the `.env` file to something like `admin:$apr1$qw47tslj$nul9I/QXX4PuVG6ivalzd0` (password is `admin`)


## Migrating data between servers (stage, prod, local)
- copy the db/ and the /apps folder to the new server
- update the rows in `wcf1_application` table
- delete the apps/cache folder contents (keep the .htaccess file)
- reload the application


## Virus Scan
- clamav is standalone and can be accessed via a Woltlab plugin (not open source, please contact Woltlab Support)
- instructions
  1. Upload / install the `attachmentAntivirus.tar` you got from Wotlab Support
  2. Go to `https://woltlab.127.0.0.1.nip.io/acp/index.php?option/60/#category_message.attachment` and set `clamd-Socket` to `tcp://clamav:3310`
  3. save and you are done!
